import { Component, LOCALE_ID, Inject } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { Storage } from '@ionic/storage';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any=HomePage;
  data:any;

  constructor(@Inject(LOCALE_ID) locale: string, platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, private storage: Storage) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
      console.log('locale', locale);
      this.storage.get('responseData').then((val) => {
        this.data = val;
        if (this.data) {
          if (this.data.usersRole_id == '1') {
            this.rootPage='UuserPage';
          }
          if (this.data.usersRole_id == '2') {
            this.rootPage='DuserPage';
          }
          if (this.data.usersRole_id == '3') {
            this.rootPage='CuserPage';
          }
        }
        else
        {
          this.rootPage = HomePage;
        }

      });

    });
  }
}

