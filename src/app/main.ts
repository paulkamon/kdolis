import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app.module';
import { LOCALE_ID } from '@angular/core';

// platformBrowserDynamic().bootstrapModule(AppModule);
platformBrowserDynamic([{provide: LOCALE_ID, useValue: 'fr'}]).bootstrapModule(AppModule, {providers: [{provide: LOCALE_ID, useValue: 'fr'}]});
