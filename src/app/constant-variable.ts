export const ConstantVariable:any = {
    //URL API
    timeOut: 15000,

    apiUrl: "http://panel.kdolis.com/kdolis/api/v1/",
    //apiUrl: "http://192.168.1.2/api/v1/",
    allSecteur: 'secteur/all',
    login: 'user/connection',
    updatePwd: 'com/password/update',
    logout: 'user/disconnect',
    register: 'buyer/register',
    achatcard: 'buyer/transaction/card/',
    cardsDeleted: 'buyer/cards/deleted/',
    cardTransactions: 'buyer/card/transaction/',
    myCardslist: 'buyer/cards',
    updateUser: 'buyer/infos/update',
    dailyTransactions: 'com/transaction/daily',
    weekTransactions: 'com/transactions/week/',
    monthTransactions: 'com/transactions/month/',
    venduesCards: 'distributeur/carte/out',
    stocklist: 'distributeur/carte/in',
    scanCard: 'com/card/read',
    bindCard: 'buyer/card/bind',
    payment: 'com/card/debit',
    activateCard: 'distributeur/carte/activation',
    order: 'client/card/request'

};