import { Injectable } from '@angular/core';
import { HTTP } from '@ionic-native/http';
// import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/timeout';
import { Storage } from '@ionic/storage';
import { ConstantVariable } from '../../app/constant-variable';
import { App } from "ionic-angular";
import { NavController } from "ionic-angular/index";

@Injectable()
export class AuthServiceProvider {
	private navCtrl: any;

	constructor(private storage: Storage,
		public http: HTTP,
		// public http: Http,
		private app:App) {
			this.navCtrl = app.getActiveNav();
	}

	  getAllSeller(id) {
		return new Promise<any>((resolve,reject) => {
			this.http.get(ConstantVariable.apiUrl + 'secteur/'+id+'/seller/all', {},{}).then((data)=>{
				let resp = JSON.parse(data.data);
				console.error(resp);
				resolve(resp);
			}).catch((error)=>{
				reject(error);
			})
		}).then((uu)=> uu)
		.catch(err => Promise.reject(err || 'err'));
	
	  }

	  getAllSecteur() {
		return new Promise<any>((resolve,reject) => {
			this.http.get(ConstantVariable.apiUrl + ConstantVariable.allSecteur, {},{}).then((data)=>{
				let resp = JSON.parse(data.data);
				console.error(resp);
				resolve(resp);
			}).catch((error)=>{
				reject(error);
			})
		}).then((uu)=> uu)
		.catch(err => Promise.reject(err || 'err'));
	
	  }

	login(body:any) {
		return new Promise<any>((resolve,reject) => {
			this.http.post(ConstantVariable.apiUrl+ConstantVariable.login, body,{}).then((data)=>{
				let resp = JSON.parse(data.data);
				console.error(resp);
				resolve(resp);
			}).catch((error)=>{
				reject(error);
			})
		}).then((uu)=> uu)
		.catch(err => Promise.reject(err || 'err'));
	}

	updatePwd(body:any) {
		return new Promise<any>((resolve,reject) => {
			this.http.post(ConstantVariable.apiUrl+ConstantVariable.updatePwd, body,{}).then((data)=>{
				let resp = JSON.parse(data.data);
				console.error(resp);
				resolve(resp);
			}).catch((error)=>{
				reject(error);
			})
		}).then((uu)=> uu)
		.catch(err => Promise.reject(err || 'err'));
	}

	logout(body:any) {
		return new Promise<any>((resolve,reject) => {
			this.http.post(ConstantVariable.apiUrl+ConstantVariable.logout, body,{}).then((data)=>{
				let resp = JSON.parse(data.data);
				console.error(resp);
				resolve(resp);
			}).catch((error)=>{
				reject(error);
			})
		}).then((uu)=> uu)
		.catch(err => Promise.reject(err || 'err'));
	}

	register(body) : Promise<any> {

		console.log(body);
		return new Promise<any>((resolve,reject) => {
			this.http.post(ConstantVariable.apiUrl+ConstantVariable.register, body,{}).then((data)=>{
				let resp = JSON.parse(data.data);
				console.error(resp);
				resolve(resp);
			})
			.catch((error)=>{
				reject(error);
			})
						
            })
            .then((uu)=> uu)
            .catch(err => Promise.reject(err || 'err'));
	}

	updateUser(body) : Promise<any> {

		console.log(body);
		return new Promise<any>((resolve,reject) => {
			this.http.post(ConstantVariable.apiUrl+ConstantVariable.updateUser, body,{}).then((data)=>{
				let resp = JSON.parse(data.data);
				console.error(resp);
				resolve(resp);
			})
			.catch((error)=>{
				reject(error);
			})
						
            })
            .then((uu)=> uu)
            .catch(err => Promise.reject(err || 'err'));
	}

	getAchatCards(id) {
		return new Promise<any>((resolve,reject) => {
			this.http.get(ConstantVariable.apiUrl + ConstantVariable.achatcard+id, {},{}).then((data)=>{
				let resp = JSON.parse(data.data);
				console.error(resp);
				resolve(resp);
			}).catch((error)=>{
				reject(error);
			})
		}).then((uu)=> uu)
		.catch(err => Promise.reject(err || 'err'));
	
	  }

	  getACardTransacions(id) {
		return new Promise<any>((resolve,reject) => {
			this.http.get(ConstantVariable.apiUrl + ConstantVariable.cardTransactions+id, {},{}).then((data)=>{
				let resp = JSON.parse(data.data);
				console.error(resp);
				resolve(resp);
			}).catch((error)=>{
				reject(error);
			})
		}).then((uu)=> uu)
		.catch(err => Promise.reject(err || 'err'));
	
	  }

	  getDeletedCards(id) {
		return new Promise<any>((resolve,reject) => {
			this.http.get(ConstantVariable.apiUrl + ConstantVariable.cardsDeleted+id, {},{}).then((data)=>{
				let resp = JSON.parse(data.data);
				console.error(resp);
				resolve(resp);
			}).catch((error)=>{
				reject(error);
			})
		}).then((uu)=> uu)
		.catch(err => Promise.reject(err || 'err'));
	
	  }

	  getCardsList(body) {
		return new Promise<any>((resolve,reject) => {
			this.http.post(ConstantVariable.apiUrl + ConstantVariable.myCardslist, body,{}).then((data)=>{
				let resp = JSON.parse(data.data);
				console.error(resp);
				resolve(resp);
			}).catch((error)=>{
				reject(error);
			})
		}).then((uu)=> uu)
		.catch(err => Promise.reject(err || 'err'));
	
	  }

	  getTransactionDaily(body) {
		return new Promise<any>((resolve,reject) => {
			this.http.post(ConstantVariable.apiUrl + ConstantVariable.dailyTransactions, body,{}).then((data)=>{
				let resp = JSON.parse(data.data);
				console.error(resp);
				resolve(resp);
			}).catch((error)=>{
				reject(error);
			})
		}).then((uu)=> uu)
		.catch(err => Promise.reject(err || 'err'));
	
	  }


	  getTransactionsWeek(id) {
		return new Promise<any>((resolve,reject) => {
			this.http.get(ConstantVariable.apiUrl + ConstantVariable.weekTransactions+id, {},{}).then((data)=>{
				let resp = JSON.parse(data.data);
				console.error(resp);
				resolve(resp);
			}).catch((error)=>{
				reject(error);
			})
		}).then((uu)=> uu)
		.catch(err => Promise.reject(err || 'err'));
	  }

	  getTransactionsMonth(id) {
		return new Promise<any>((resolve,reject) => {
			this.http.get(ConstantVariable.apiUrl + ConstantVariable.monthTransactions+id, {},{}).then((data)=>{
				let resp = JSON.parse(data.data);
				console.error(resp);
				resolve(resp);
			}).catch((error)=>{
				reject(error);
			})
		}).then((uu)=> uu)
		.catch(err => Promise.reject(err || 'err'));
	  }

	  getVenduesCards(body) {
		return new Promise<any>((resolve,reject) => {
			this.http.post(ConstantVariable.apiUrl + ConstantVariable.venduesCards, body,{}).then((data)=>{
				let resp = JSON.parse(data.data);
				console.error(resp);
				resolve(resp);
			}).catch((error)=>{
				reject(error);
			})
		}).then((uu)=> uu)
		.catch(err => Promise.reject(err || 'err'));
	
	  }

	  getStockCards(body) {
		return new Promise<any>((resolve,reject) => {
			this.http.post(ConstantVariable.apiUrl + ConstantVariable.stocklist, body,{}).then((data)=>{
				let resp = JSON.parse(data.data);
				console.error(resp);
				resolve(resp);
			}).catch((error)=>{
				reject(error);
			})
		}).then((uu)=> uu)
		.catch(err => Promise.reject(err || 'err'));
	
	  }


	  scanCard(body:any) {
		return new Promise<any>((resolve,reject) => {
			this.http.post(ConstantVariable.apiUrl+ConstantVariable.scanCard, body,{}).then((data)=>{
				let resp = JSON.parse(data.data);
				console.error(resp);
				resolve(resp);
			}).catch((error)=>{
				reject(error);
			})
		}).then((uu)=> uu)
		.catch(err => Promise.reject(err || 'err'));
	}

	bindCard(body:any) {
		return new Promise<any>((resolve,reject) => {
			this.http.post(ConstantVariable.apiUrl+ConstantVariable.bindCard, body,{}).then((data)=>{
				let resp = JSON.parse(data.data);
				console.error(resp);
				resolve(resp);
			}).catch((error)=>{
				reject(error);
			})
		}).then((uu)=> uu)
		.catch(err => Promise.reject(err || 'err'));
	}

	payment(body:any) {
		return new Promise<any>((resolve,reject) => {
			this.http.post(ConstantVariable.apiUrl+ConstantVariable.payment, body,{}).then((data)=>{
				let resp = JSON.parse(data.data);
				console.error(resp);
				resolve(resp);
			}).catch((error)=>{
				reject(error);
			})
		}).then((uu)=> uu)
		.catch(err => Promise.reject(err || 'err'));
	}

	activateCard(body:any) {
		return new Promise<any>((resolve,reject) => {
			this.http.post(ConstantVariable.apiUrl+ConstantVariable.activateCard, body,{}).then((data)=>{
				let resp = JSON.parse(data.data);
				console.error(resp);
				resolve(resp);
			}).catch((error)=>{
				reject(error);
			})
		}).then((uu)=> uu)
		.catch(err => Promise.reject(err || 'err'));
	}

	order(body) : Promise<any> {

		console.log(body);
		return new Promise<any>((resolve,reject) => {
			this.http.post(ConstantVariable.apiUrl+ConstantVariable.order, body,{}).then((data)=>{
				let resp = JSON.parse(data.data);
				console.error(resp);
				resolve(resp);
			})
			.catch((error)=>{
				reject(error);
			})
						
            })
            .then((uu)=> uu)
            .catch(err => Promise.reject(err || 'err'));
	}
	// resetPwd(body) : Promise<any> {

	// 	console.log(body);
	// 	return new Promise<any>((resolve,reject) => {
	// 		this.http.post(ConstantVariable.apiUrl+ConstantVariable.passwordRecovery, body,{}).then((data)=>{
	// 			let resp = JSON.parse(data.data);
	// 			console.error(resp);
	// 			resolve(resp);
	// 		})
	// 		.catch((error)=>{
	// 			reject(error);
	// 		})
						
    //         })
    //         .then((uu)=> uu)
    //         .catch(err => Promise.reject(err || 'err'));
	// }
}