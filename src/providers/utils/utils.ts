import { Injectable } from '@angular/core';
import { Toast, ToastController, Alert, AlertController, Loading, LoadingController, ActionSheetController, ActionSheet } from 'ionic-angular';

/*
  Generated class for the UtilsProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class UtilsProvider {

	constructor(private toastCtrl: ToastController,
		private alertCtrl: AlertController,
		private actionSheetCtrl: ActionSheetController,
		private loadingCtrl: LoadingController) {

	}

	makeToast(msg: string, duration: number=3000, pos: string='bottom') : Toast {
	  let toast = this.toastCtrl.create({
	    message: msg,
	    duration: duration,
	    position: pos
	  });

	  return toast;
	}

	makeAlert(title: string, msg: string, subTitle: any = null, inputs: any[]=[],btns: any[]=['Ok']) : Alert {
	  let alert = this.alertCtrl.create({
	  	title: title,
    	subTitle: subTitle,
	    message: msg,
	    inputs: inputs,
	    buttons: btns
	  });

	  return alert;
	}

	makeActionSheet(title: string, btns: any[]=['Ok']) : ActionSheet {

		btns.push({
			text: 'Annuler',
			role: 'cancel'
		});

		let actionSheet = this.actionSheetCtrl.create({
			title: title,
			buttons: btns
		});

	  return actionSheet;
	}

	makeLoader(content: string) : Loading {
	  let loader = this.loadingCtrl.create({
	  	content: content
	  });

	  return loader;
	}

}
