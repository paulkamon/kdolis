import { Injectable } from '@angular/core';
// import { HTTP } from '@ionic-native/http';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/timeout';
import { ConstantVariable } from '../../app/constant-variable';
// import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { UtilsProvider } from '../utils/utils';
import { Storage } from '@ionic/storage';

/*
  Generated class for the UserProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class UserProvider {
  token;

  constructor(public http: Http, public storage: Storage) {
    console.log('Hello UserProvider Provider');
    this.storage.get('token').then((token)=>{
      console.log(token);
      this.token = token;
      })
  }


}
