import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController} from 'ionic-angular';
import { BarcodeScanner ,BarcodeScannerOptions} from '@ionic-native/barcode-scanner';
import { Storage } from '@ionic/storage';
import { ConnectivityProvider } from '../../providers/connectivity/connectivity';
import { UtilsProvider } from '../../providers/utils/utils';
import { HomePage } from '../home/home';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';

/**
 * Generated class for the DuserPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-duser',
  templateUrl: 'duser.html',
})
export class DuserPage {
  data= {
    userId: ''
    };
    reponseData:any[];
    data2={
      comId: ''
    };
    options :BarcodeScannerOptions;

  constructor(public navCtrl: NavController, public navParams: NavParams, public storage: Storage,
    private connectivityService: ConnectivityProvider, public alertCtrl: AlertController,
     private barcodeScanner: BarcodeScanner,private utils: UtilsProvider,private authService: AuthServiceProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DuserPage');
    this.storage.get('userData').then((data)=>{
      this.data2.comId =data.id;
      this.getTransactionDaily(this.data2);
    });
  }
  // ionViewDidEnter(){
  //   this.getTransactionDaily(this.data2);
  // }

  scan(){
    this.options = {
      prompt : "Scan your barcode ",
      disableSuccessBeep: true,
  }
    this.barcodeScanner.scan(this.options).then(barcodeData => {
      console.log('Barcode data', barcodeData);
      this.scancard(barcodeData.text);
     }).catch(err => {
         console.log('Error', err);
     });
    // this.navCtrl.push("ScanPage");
  }

  dailyT(){
    this.navCtrl.push('TransactionDailyPage');
  }

  allT(){
    this.navCtrl.push('TransactionAllPage');
  }

  getTransactionDaily(data){
    if(this.connectivityService.isOnline()){
      let loader = this.utils.makeLoader('loading...');
      loader.present().then(()=>{
        this.authService.getTransactionDaily(data).then(response =>{
            loader.dismiss();
            this.reponseData = response.data;
            console.log(response);
        }).catch((error)=>{
          console.log('error'+error);
  
          loader.dismiss();
        })
      })
    }
    else{
      this.utils.makeToast('Veuillez vous connecter à internet').present();
    }
  }

  scancard(code) {
    if(this.connectivityService.isOnline()){
      let loader = this.utils.makeLoader('');
      loader.present().then(()=>{
        let data = {
          cardCode: code
          };
        this.authService.scanCard(data).then(response =>{
            loader.dismiss();
            console.log(response);
            if (response.code==1) {
              this.navCtrl.push('ScanPage',{card: response.data, duser:true}) ; 
            }else{
              this.utils.makeToast(response.msg).present();
            }
          
        }).catch((error)=>{
          console.log(error);
  
          loader.dismiss();
          this.utils.makeToast('erreur!').present();
        })
      })
    }
    else{
      this.utils.makeToast('Veuillez vous connecter à internet').present();
    }
  }

  showConfirm() {
    console.log('showconfirm');
    const confirm = this.alertCtrl.create({
      title: 'Deconnexion?',
      message: 'voulez vous vraiment vous déconnecter?',
      buttons: [
        {
          text: 'non',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'oui',
          handler: () => {
            console.log('Agree clicked');
            this.logout();
          }
        }
      ]
    });
    confirm.present();
  }
  
  disconnect(){
    this.storage.clear().then(()=>{
      this.navCtrl.setRoot(HomePage);
      console.log('cleared');
    })
    .catch(()=>{
      this.utils.makeToast('Veuillez réessayer svp!').present();
    });
  }

  logout(){
    if(this.connectivityService.isOnline()){
      let loader = this.utils.makeLoader('');
      loader.present().then(()=>{
        this.authService.logout(this.data).then(response =>{
            loader.dismiss();
            console.log(response);
            if (response.code==1) {
              this.utils.makeToast("Utilisateur déconnecté").present();
              this.disconnect();
            }else{
              this.utils.makeToast(response.msg).present();
            }
          
        }).catch((error)=>{
          console.log('erreur '+JSON.stringify(error) );
  
          loader.dismiss();
          this.utils.makeToast('erreur!').present();
        })
      })
    }
    else{
      this.utils.makeToast('Veuillez vous connecter à internet').present();
    }
  }
}
