import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DuserPage } from './duser';

@NgModule({
  declarations: [
    DuserPage,
  ],
  imports: [
    IonicPageModule.forChild(DuserPage),
  ],
})
export class DuserPageModule {}
