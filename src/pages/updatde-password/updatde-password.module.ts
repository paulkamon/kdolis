import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UpdatdePasswordPage } from './updatde-password';

@NgModule({
  declarations: [
    UpdatdePasswordPage,
  ],
  imports: [
    IonicPageModule.forChild(UpdatdePasswordPage),
  ],
})
export class UpdatdePasswordPageModule {}
