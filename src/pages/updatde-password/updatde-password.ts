import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,AlertController } from 'ionic-angular';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { ConnectivityProvider } from '../../providers/connectivity/connectivity';
import { UtilsProvider } from '../../providers/utils/utils';
import { Storage } from '@ionic/storage';
/**
 * Generated class for the UpdatdePasswordPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-updatde-password',
  templateUrl: 'updatde-password.html',
})
export class UpdatdePasswordPage {

  response:any;
  password;
  confirmPassword;

  constructor(public navCtrl: NavController, public navParams: NavParams, private connectivityService: ConnectivityProvider,
    private authService: AuthServiceProvider,
    private utils: UtilsProvider,
    public storage: Storage,
    private alertCtrl: AlertController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad UpdatdePasswordPage');
    this.response = this.navParams.get('response');
  }

  back(){
    this.navCtrl.pop();
  }
updateClick(){
  if (this.password != '' && this.password == this.confirmPassword) {
  
  let comdata = {
    comUserId: this.response.data.userInfos.id,
    ComNewPassword: this.password
    };

    let disdata = {
      distUserId: this.response.data.userInfos.id,
      distNewPassword: this.password
      };
      
      if(this.response.data.usersRole_id == '2'){
        this.update(comdata);
        console.log(JSON.stringify(comdata));
      }
      if(this.response.data.usersRole_id == '3'){
        this.update(disdata);
        console.log(JSON.stringify(disdata));
      }
        
  } else {
    
  }
}

  update(data) {
    if(this.connectivityService.isOnline()){
      let loader = this.utils.makeLoader('modification en cours...');
      loader.present().then(()=>{
        this.authService.login(data).then(reponse =>{
            loader.dismiss();
            // this.response = response;
            console.log(reponse);
            if (reponse.code==1) {
              this.storage.set('connexion', this.response.connexion).then(()=>{
                console.log('connexionstored');
            this.storage.set('userData', this.response.data.userInfos).then(()=>{
              console.log('userdatastored');
            this.storage.set('responseData', this.response.data).then(()=>{
                console.log('stored');
                  if(this.response.data.usersRole_id == '2'){
                    this.navCtrl.setRoot('DuserPage');
                  }
                  if(this.response.data.usersRole_id == '3'){
                    this.navCtrl.setRoot('CuserPage');
                  }
  
              });
            });
          });
        }else{
              this.utils.makeToast(reponse.msg).present();
            }
          
        }).catch((error)=>{
          console.log(error);
  
          loader.dismiss();
          this.utils.makeToast('veuillez réesayer!').present();
        })
      })
    }
    else{
      this.utils.makeToast('Veuillez vous connecter à internet').present();
    }
  }
}
