import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TransactionDailyPage } from './transaction-daily';

@NgModule({
  declarations: [
    TransactionDailyPage,
  ],
  imports: [
    IonicPageModule.forChild(TransactionDailyPage),
  ],
})
export class TransactionDailyPageModule {}
