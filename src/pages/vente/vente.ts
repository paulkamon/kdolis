import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { ConnectivityProvider } from '../../providers/connectivity/connectivity';
import { UtilsProvider } from '../../providers/utils/utils';
import { Storage } from '@ionic/storage';
/**
 * Generated class for the VentePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-vente',
  templateUrl: 'vente.html',
})
export class VentePage {

  data = {
    clientOrder: {
      forfaitList: [{
        created_at: '2018-10-11 21:06:43',
        deleted_at: '',
        id: '5',
        label: 'Saphir',
        montant: '10000.00',
        quantity: 0,
        selected: false,
        status: 'active',
        updated_at: '2018-10-18 13:52:44'
      }, 
      {created_at: '2018-10-11 21:07:52',
        deleted_at: '',
        id: '6',
        label: 'Rubis',
        montant: '25000.00',
        quantity: 0,
        selected: false,
        status: 'active',
        updated_at: '2018-10-18 13:53:44'
      },
      {created_at: '2018-10-11 21:09:16',
        deleted_at: '',
        id: '7',
        label: 'Emeraude',
        montant: '50000.00',
        quantity: 0,
        selected: false,
        status: 'active',
        updated_at: '2018-10-18 13:51:59'
      },{
        created_at: '2018-10-11 21:10:02',
        deleted_at: '',
        id: '8',
        label: 'Diamant',
        montant: '100000.00',
        quantity: 0,
        selected: false,
        status: 'active',
        updated_at: '2018-10-18 13:52:54'
      }],
      orderCard: '',
      orderCardNumber: 0,
      orderDeliveryDate: '',
      orderName: '',
      orderNumber: '',
      orderResidence: '',
      orderSurname: ''
      }
    };

  constructor(public navCtrl: NavController, public navParams: NavParams, private connectivityService: ConnectivityProvider,
    private authService: AuthServiceProvider,
    private utils: UtilsProvider,
    public storage: Storage) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad VentePage');
  }

  back(){
    this.navCtrl.pop();
  }


  order() {
    console.log(JSON.stringify(this.data));

    if (this.data.clientOrder.orderNumber && this.data.clientOrder.orderSurname) {
    if (this.data.clientOrder.forfaitList[0].quantity>0 || this.data.clientOrder.forfaitList[1].quantity>0 || this.data.clientOrder.forfaitList[2].quantity>0 || this.data.clientOrder.forfaitList[3].quantity>0) {
    if(this.connectivityService.isOnline()){
      let loader = this.utils.makeLoader('');
      loader.present().then(()=>{
        this.authService.order(this.data).then(response =>{
            loader.dismiss();
            // this.response = response;
            console.log(response);
            // if (response.code==1) {
              this.utils.makeToast('Commande effectuée avec succes').present();
              this.navCtrl.pop();
            // }else{
            //   this.utils.makeToast(response.msg).present();
            // }
          
        }).catch((error)=>{
          console.log(error);
  
          loader.dismiss();
          this.utils.makeToast('erreur!').present();
        })
      })
    }
    else{
      this.utils.makeToast('Veuillez vous connecter à internet').present();
    }
  } else {
    this.utils.makeToast('veuillez selectionner au moins une carte').present();
  }
} else {
  this.utils.makeToast('veuillez entrer vos informations personnelles').present();
}
  }
}
