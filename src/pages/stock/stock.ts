import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { ConnectivityProvider } from '../../providers/connectivity/connectivity';
import { UtilsProvider } from '../../providers/utils/utils';
import { Storage } from '@ionic/storage';
/**
 * Generated class for the StockPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-stock',
  templateUrl: 'stock.html',
})
export class StockPage {
  user;
  reponseData:any[];
  data={
    distributeurInfosId:''
  };
  constructor(public navCtrl: NavController, public navParams: NavParams, private connectivityService: ConnectivityProvider,
    private authService: AuthServiceProvider,
    private utils: UtilsProvider, public storage: Storage) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad StockPage');
    this.storage.get('userData').then((data)=>{
      this.user = data;
      this.data.distributeurInfosId=data.id;
      this.getStockCards();
    });
  }

  back(){
    this.navCtrl.pop();
  }

  getStockCards(){
    if(this.connectivityService.isOnline()){
      let loader = this.utils.makeLoader('loading...');
      loader.present().then(()=>{
        this.authService.getStockCards(this.data).then(response =>{
            loader.dismiss();
            this.reponseData = response.data;
            console.log(response);
        }).catch((error)=>{
          console.log('error'+error);
  
          loader.dismiss();
        })
      })
    }
    else{
      this.utils.makeToast('Veuillez vous connecter à internet').present();
    }
  }
}
