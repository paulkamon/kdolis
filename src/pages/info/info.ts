import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { ConnectivityProvider } from '../../providers/connectivity/connectivity';
import { UtilsProvider } from '../../providers/utils/utils';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the InfoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-info',
  templateUrl: 'info.html',
})
export class InfoPage {
  user;

  distributeur;

  nom:string;
  prenoms:string;
  date;
  telephone;
  login;
  email;

  data = {
    buyerId: '',
    emailNew: '',
    passwordNew: '',
    contactdNew: ''
  };
  

  constructor(public navCtrl: NavController, public navParams: NavParams, private connectivityService: ConnectivityProvider,
    private authService: AuthServiceProvider,
    private utils: UtilsProvider,
    public storage: Storage) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad InfoPage');
    this.storage.get('userData').then((data)=>{
      console.log('userData '+JSON.stringify(data));
      this.user = data;
      this.nom = data.nom;
      this.prenoms = data.prenoms;
      this.email = data.email;
      this.date =new Date(data.created_at.replace(/-/g, '/'));;
      this.telephone = data.telephone;
      this.storage.get('responseData').then((data)=>{
        this.login = data.login;
        this.data.buyerId = data.id;
        // this.data.contactdNew = this.telephone;
        // this.data.emailNew = this.email;
        if (data.usersRole_id != 3 || data.usersRole_id != '3') {
          this.distributeur = true;
        }
      // this.updateUser(this.user.users_id);
    });
  });
  }

  back(){
    this.navCtrl.pop();
  }

  updateUser() {
    
    if(this.connectivityService.isOnline()){
      if (this.data.buyerId && this.data.emailNew) {
      let loader = this.utils.makeLoader('Connexion en cours...');
      loader.present().then(()=>{
        this.authService.updateUser(this.data).then(response =>{
            loader.dismiss();
            // this.response = response;
            console.log(response);
            if (response.code==1) {
              
              this.utils.makeToast('Mise à jour des informations Réussie').present();

            }else{
              this.utils.makeToast(response.msg).present();
            }
          
        }).catch((error)=>{
          console.log(error);
  
          loader.dismiss();
          this.utils.makeToast('erreur!').present();
        })
      })
    }else{
      this.utils.makeToast('Veuillez remplir les champ').present();

    }
  }
    else{
      this.utils.makeToast('Veuillez vous connecter à internet').present();
    }
  }
}
