import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { VenduesPage } from './vendues';

@NgModule({
  declarations: [
    VenduesPage,
  ],
  imports: [
    IonicPageModule.forChild(VenduesPage),
  ],
})
export class VenduesPageModule {}
