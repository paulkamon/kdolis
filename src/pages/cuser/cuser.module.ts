import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CuserPage } from './cuser';

@NgModule({
  declarations: [
    CuserPage,
  ],
  imports: [
    IonicPageModule.forChild(CuserPage),
  ],
})
export class CuserPageModule {}
