import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { ConnectivityProvider } from '../../providers/connectivity/connectivity';
import { UtilsProvider } from '../../providers/utils/utils';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController, private barcodeScanner: BarcodeScanner, private connectivityService: ConnectivityProvider,
    private authService: AuthServiceProvider,
    private utils: UtilsProvider) {

  }

  com(){
    this.navCtrl.push("CommercePage");
  }

vente(){
  this.navCtrl.push("VentePage");
}

scan(){

  this.barcodeScanner.scan().then(barcodeData => {
    console.log('Barcode data', barcodeData);
    this.scancard(barcodeData.text);
   }).catch(err => {
       console.log('Error', err);
   });
  // this.navCtrl.push("ScanPage");
}

connect(){
  this.navCtrl.push("ConnectPage");
}


scancard(code) {
  if(this.connectivityService.isOnline()){
    let loader = this.utils.makeLoader('');
    loader.present().then(()=>{
      let data = {
        cardCode: code
        };
      this.authService.scanCard(data).then(response =>{
          loader.dismiss();
          console.log(response);
          if (response.code==1) {
            this.navCtrl.push('ScanPage',{card: response.data , home:true}) ; 
          }else{
            this.utils.makeToast(response.msg).present();
          }
        
      }).catch((error)=>{
        console.log(error);

        loader.dismiss();
        this.utils.makeToast('erreur!').present();
      })
    })
  }
  else{
    this.utils.makeToast('Veuillez vous connecter à internet').present();
  }
}

}
