import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AchatDetailPage } from './achat-detail';

@NgModule({
  declarations: [
    AchatDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(AchatDetailPage),
  ],
})
export class AchatDetailPageModule {}
