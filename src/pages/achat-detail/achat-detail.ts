import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { ConnectivityProvider } from '../../providers/connectivity/connectivity';
import { UtilsProvider } from '../../providers/utils/utils';

/**
 * Generated class for the AchatDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-achat-detail',
  templateUrl: 'achat-detail.html',
})
export class AchatDetailPage {
reponseData:any[];
card;
cardno;
solde;

  constructor(public navCtrl: NavController, public navParams: NavParams, private connectivityService: ConnectivityProvider,
    private authService: AuthServiceProvider,
    private utils: UtilsProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AchatDetailPage');
    this.card = this.navParams.get('card');
    this.cardno = this.card.numCarte;
    this.solde = this.card.solde;

    this.getCardTransaction(this.card.id);
  }

  back(){
    this.navCtrl.pop();
  }

  public convertTodate(string){
    return new Date(string.replace(/-/g, '/'));
  }
  public round(decimal){
    return Math.round(decimal);
  }

  getCardTransaction(id){
    if(this.connectivityService.isOnline()){
      let loader = this.utils.makeLoader('loading...');
      loader.present().then(()=>{
        this.authService.getACardTransacions(id).then(response =>{
            loader.dismiss();
            this.reponseData = response.data;
            console.log(response);
        }).catch((error)=>{
          console.log('error'+error);
  
          loader.dismiss();
        })
      })
    }
    else{
      this.utils.makeToast('Veuillez vous connecter à internet').present();
    }
  }

}
