import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams,AlertController } from 'ionic-angular';
import JsBarcode from 'jsbarcode';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { ConnectivityProvider } from '../../providers/connectivity/connectivity';
import { UtilsProvider } from '../../providers/utils/utils';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the ScanPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-scan',
  templateUrl: 'scan.html',
})
export class ScanPage {

  public picture;
  @ViewChild('barcode') barcode: ElementRef;

card;

cardno;
solde;
forfait;
expiration;
home=true;
duser;
cuser;

data = {
  cardId: '',
  montantDebit: '',
  comId: ''
  };

  dataDist = {
    type: 'dist',
    cardId: '',
    distributeurPass: '',
    distributeurId: '',
    userId: ''
    };

demande;
activate;
userdata;

  constructor(public navCtrl: NavController, public navParams: NavParams, private connectivityService: ConnectivityProvider,
    private authService: AuthServiceProvider,
    private utils: UtilsProvider, public alertCtrl: AlertController,public storage: Storage) {
  }

  ionViewDidLoad() {
this.storage.get('userData').then((data)=>{
  this.userdata = data;
  this.data.comId = data.id;
  this.dataDist.distributeurId = data.id;
  this.dataDist.userId = data.users_id;
  console.log('card '+JSON.stringify(this.card));
  console.log('userData '+JSON.stringify(data));

  console.log('ionViewDidLoad ScanPage');
    this.card = this.navParams.get('card');
    this.home = this.navParams.get('home');
    this.duser = this.navParams.get('duser');
    this.cuser = this.navParams.get('cuser');
    this.cardno = this.card.numCarte;
    this.solde = this.card.solde;
    this.forfait = this.card.forfait;
    this.data.cardId = this.card.id;
    this.dataDist.cardId = this.card.id;

    var date1 =new Date(this.card.dateExpirated.replace(/-/g, '/'));
    var date2 = new Date();
    var Difference_In_Time = date1.getTime() - date2.getTime();
    this.expiration =Math.round(Difference_In_Time / (1000 * 3600 * 24));
    
    
    JsBarcode(this.barcode.nativeElement, this.cardno, {
  height:40,
  displayValue: false
});

}).catch((err) =>console.log('err '+JSON.stringify(err)));

  }

  back(){
    this.navCtrl.pop();
  }

  demandPay(){
    if (!this.demande) {
      this.demande=true;
      return;
    }
    if (this.demande && this.data.montantDebit) {
      this.showConfirm();
    }
  }

  demandActivation(){
    console.log('card '+JSON.stringify(this.card));
    console.log('userData '+JSON.stringify(this.userdata));
    if (!this.activate) {
      this.activate=true;
      return;
    }
    if (this.activate && this.dataDist.distributeurPass) {
      this.activation();
    }
  }

  showConfirm() {
    console.log('showconfirm');
    const confirm = this.alertCtrl.create({
      // title: 'Deconnexion?',
      message: 'EFFECTUER LE PAIEMENT DE '+this.data.montantDebit+' F ?',
      buttons: [
        {
          text: 'ANNULER',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'CONFIRMER',
          handler: () => {
            console.log('Agree clicked');
            this.pay();
          }
        }
      ]
    });
    confirm.present();
  }

  pay(){
    if(this.connectivityService.isOnline()){
      let loader = this.utils.makeLoader('');
      loader.present().then(()=>{
        this.authService.payment(this.data).then(response =>{
            loader.dismiss();
            console.log(response);
            if (response.code==1) {
              this.utils.makeToast(response.msg).present();
              this.demande=false;
            }else{
              this.utils.makeToast('impossible '+response.msg).present();
            }
          
        }).catch((error)=>{
          console.log('erreur '+JSON.stringify(error) );
  
          loader.dismiss();
          this.utils.makeToast('erreur!').present();
        })
      })
    }
    else{
      this.utils.makeToast('Veuillez vous connecter à internet').present();
    }
  }

  activation(){
    console.log(JSON.stringify(this.dataDist));
    if(this.connectivityService.isOnline()){
      let loader = this.utils.makeLoader('');
      loader.present().then(()=>{
        this.authService.activateCard(this.dataDist).then(response =>{
            loader.dismiss();
            console.log(response);
            if (response.code==1) {
              this.utils.makeToast(response.msg).present();
              alert("Carte activée avec succès")
              this.activate=false;
            }else{
              this.utils.makeToast('impossible '+response.msg).present();
              alert('impossible !'+response.msg)
            }
          
        }).catch((error)=>{
          console.log('erreur '+JSON.stringify(error) );
  
          loader.dismiss();
          this.utils.makeToast('erreur!').present();
        })
      })
    }
    else{
      this.utils.makeToast('Veuillez vous connecter à internet').present();
    }
  }
}
