import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,AlertController  } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { ConnectivityProvider } from '../../providers/connectivity/connectivity';
import { UtilsProvider } from '../../providers/utils/utils';
import { HomePage } from '../home/home';
import { BarcodeScanner ,BarcodeScannerOptions} from '@ionic-native/barcode-scanner';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';

/**
 * Generated class for the UuserPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-uuser',
  templateUrl: 'uuser.html',
})
export class UuserPage {
user;
data= {
  userId: ''
  };
options :BarcodeScannerOptions;

nom:string;
prenoms:string;
date;
today: number = Date.now();

  constructor(public navCtrl: NavController, public navParams: NavParams, public storage: Storage,
    private connectivityService: ConnectivityProvider,
    private utils: UtilsProvider, public alertCtrl: AlertController, private barcodeScanner: BarcodeScanner,
    private authService: AuthServiceProvider) {
  }
  
  ionViewDidLoad() {
    console.log('ionViewDidLoad UuserPage');
    console.log('today'+this.today);
    this.storage.get('userData').then((data)=>{
      this.user = data;
      this.nom = data.nom;
      this.prenoms = data.prenoms;

      this.data.userId= data.users_id;

      this.storage.get('connexion').then((connexion)=>{
        this.date =new Date(connexion.replace(/-/g, '/'));;
        console.log('nom '+this.user.nom);
      });
    });
  }

  scan(){
    this.options = {
      prompt : "Scan your barcode ",
      disableSuccessBeep: true,
  }
    this.barcodeScanner.scan(this.options).then(barcodeData => {
      console.log('Barcode data', barcodeData);
      this.scancard(barcodeData.text);
     }).catch(err => {
         console.log('Error', err);
     });
    // this.navCtrl.push("ScanPage");
  }

achats(){
this.navCtrl.push("AchatPage");
}

corb(){
this.navCtrl.push("CorbeillePage");

}

info(){
this.navCtrl.push("InfoPage");

}

port(){
this.navCtrl.push("PrtfeuillePage");
}


scancard(code) {
  if(this.connectivityService.isOnline()){
    let loader = this.utils.makeLoader('');
    loader.present().then(()=>{
      let data = {
        cardCode: code
        };
      this.authService.scanCard(data).then(response =>{
          loader.dismiss();
          console.log(response);
          if (response.code==1) {
            this.navCtrl.push('PrtfeuilleDetailPage',{card: response.data , scan:true}) ; 
          }else{
            this.utils.makeToast(response.msg).present();
          }
        
      }).catch((error)=>{
        console.log(error);

        loader.dismiss();
        this.utils.makeToast('erreur!').present();
      })
    })
  }
  else{
    this.utils.makeToast('Veuillez vous connecter à internet').present();
  }
}

showConfirm() {
  console.log('showconfirm');
  const confirm = this.alertCtrl.create({
    title: 'Deconnexion?',
    message: 'voulez vous vraiment vous déconnecter?',
    buttons: [
      {
        text: 'non',
        handler: () => {
          console.log('Disagree clicked');
        }
      },
      {
        text: 'oui',
        handler: () => {
          console.log('Agree clicked');
          this.logout();
        }
      }
    ]
  });
  confirm.present();
}

disconnect(){
  this.storage.clear().then(()=>{
    this.navCtrl.setRoot(HomePage);
    console.log('cleared');
  })
  .catch(()=>{
    this.utils.makeToast('Veuillez réessayer svp!').present();
  });
}

logout(){
  if(this.connectivityService.isOnline()){
    let loader = this.utils.makeLoader('');
    loader.present().then(()=>{
      this.authService.logout(this.data).then(response =>{
          loader.dismiss();
          console.log(response);
          if (response.code==1) {
            this.utils.makeToast("Utilisateur déconnecté").present();
            this.disconnect();
          }else{
            this.utils.makeToast(response.msg).present();
          }
        
      }).catch((error)=>{
        console.log('erreur '+JSON.stringify(error) );

        loader.dismiss();
        this.utils.makeToast('erreur!').present();
      })
    })
  }
  else{
    this.utils.makeToast('Veuillez vous connecter à internet').present();
  }
}
}
