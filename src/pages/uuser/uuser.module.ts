import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UuserPage } from './uuser';

@NgModule({
  declarations: [
    UuserPage,
  ],
  imports: [
    IonicPageModule.forChild(UuserPage),
  ],
})
export class UuserPageModule {}
