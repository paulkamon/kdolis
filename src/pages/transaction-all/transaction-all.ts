import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { ConnectivityProvider } from '../../providers/connectivity/connectivity';
import { UtilsProvider } from '../../providers/utils/utils';
import { Storage } from '@ionic/storage';
/**
 * Generated class for the TransactionAllPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-transaction-all',
  templateUrl: 'transaction-all.html',
})
export class TransactionAllPage {
  user;
  reponseData:any[];
  reponseData2:any[];   
  transaction;

  constructor(public navCtrl: NavController, public navParams: NavParams, private connectivityService: ConnectivityProvider,
    private authService: AuthServiceProvider,
    private utils: UtilsProvider, public storage: Storage) {
      this.transaction="week"
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TransactionAllPage');
    this.storage.get('userData').then((data)=>{
      this.user = data;
      this.getTransactionsWeek(this.user.id);
      this.getTransactionsMonth(this.user.id);
    });
  }

  back(){
    this.navCtrl.pop();
  }

  getTransactionsWeek(id){
    if(this.connectivityService.isOnline()){
      let loader = this.utils.makeLoader('loading...');
      loader.present().then(()=>{
        this.authService.getTransactionsWeek(id).then(response =>{
          
          this.getTransactionsMonth(this.user.id);

            loader.dismiss();
            this.reponseData = response.data;
            console.log(response);
        }).catch((error)=>{
          console.log('error'+error);
  
          loader.dismiss();
        })
      })
    }
    else{
      this.utils.makeToast('Veuillez vous connecter à internet').present();
    }
  }

  getTransactionsMonth(id){
    if(this.connectivityService.isOnline()){
      let loader = this.utils.makeLoader('loading...');
      loader.present().then(()=>{
        this.authService.getTransactionsMonth(id).then(response =>{
            loader.dismiss();
            this.reponseData2 = response.data;
            console.log(response);
        }).catch((error)=>{
          console.log('error'+error);
  
          loader.dismiss();
        })
      })
    }
    else{
      this.utils.makeToast('Veuillez vous connecter à internet').present();
    }
  }
}
