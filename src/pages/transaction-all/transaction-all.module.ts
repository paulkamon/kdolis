import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TransactionAllPage } from './transaction-all';

@NgModule({
  declarations: [
    TransactionAllPage,
  ],
  imports: [
    IonicPageModule.forChild(TransactionAllPage),
  ],
})
export class TransactionAllPageModule {}
