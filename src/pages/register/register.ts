import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { ConnectivityProvider } from '../../providers/connectivity/connectivity';
import { UtilsProvider } from '../../providers/utils/utils';
import { Storage } from '@ionic/storage';
/**
 * Generated class for the RegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {
  isAccepted: boolean = false;
  data = {
    buyerInfos: {
      nom: '',
      prenoms: '',
      email: '',
      telephone: '',
      telephone2: '',
      login: '',
      password: '',
      }
    };

  constructor(public navCtrl: NavController, public navParams: NavParams, private connectivityService: ConnectivityProvider,
    private authService: AuthServiceProvider,
    private utils: UtilsProvider,
    public storage: Storage) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterPage');
  }

  user(){

    this.navCtrl.setRoot("UuserPage")
    
  }

  register() {
    
    if (this.data.buyerInfos.nom && this.data.buyerInfos.email && this.data.buyerInfos.login && this.data.buyerInfos.password) {
      if (this.isAccepted) {
    if(this.connectivityService.isOnline()){
      let loader = this.utils.makeLoader('Connexion en cours...');
      loader.present().then(()=>{
        this.authService.register(this.data).then(response =>{
            loader.dismiss();
            // this.response = response;
            console.log(response);
            if (response.code==1) {
              
            let data2 = {
              login: this.data.buyerInfos.login,
              password: this.data.buyerInfos.password
              };
              this.login(data2)

            }else{
              this.utils.makeToast(response.msg).present();
            }
          
        }).catch((error)=>{
          console.log(error);
  
          loader.dismiss();
          this.utils.makeToast('erreur!').present();
        })
      })
    }
    else{
      this.utils.makeToast('Veuillez vous connecter à internet').present();
    }
  }
}else{
  this.utils.makeToast('Veuillez remplir les champs').present();
}
  }

  back(){
    this.navCtrl.pop();
  }


  login(data) {
    if(this.connectivityService.isOnline()){
      let loader = this.utils.makeLoader('Connexion en cours...');
      loader.present().then(()=>{
        this.authService.login(data).then(response =>{
            loader.dismiss();
            // this.response = response;
            console.log(response);
            if (response.code==1) {
              this.storage.set('connexion', response.connexion).then(()=>{
                console.log('connexionstored');
            this.storage.set('userData', response.data.userInfos).then(()=>{
              console.log('userdatastored');
            this.storage.set('responseData', response.data).then(()=>{
                console.log('stored');
                  // this.navCtrl.setRoot(TabsPage,{});
                  if(response.data.usersRole_id == '1'){
                    this.navCtrl.setRoot('UuserPage');
                  }
                  if(response.data.usersRole_id == '2'){
                    this.navCtrl.setRoot('DuserPage');
                  }
  
              });
            });
          });
           
        }else{
              this.utils.makeToast(response.msg).present();
            }
          
        }).catch((error)=>{
          console.log(error);
  
          loader.dismiss();
          this.utils.makeToast('Votre login ou votre mot passe est incorrecte!').present();
        })
      })
    }
    else{
      this.utils.makeToast('Veuillez vous connecter à internet').present();
    }
  }
}
