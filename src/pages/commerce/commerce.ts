import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { ConnectivityProvider } from '../../providers/connectivity/connectivity';
import { UtilsProvider } from '../../providers/utils/utils';
import { Storage } from '@ionic/storage';
/**
 * Generated class for the CommercePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-commerce',
  templateUrl: 'commerce.html',
})
export class CommercePage {

  reponseData:any[];

  constructor(public navCtrl: NavController, public navParams: NavParams, private connectivityService: ConnectivityProvider,
    private authService: AuthServiceProvider,
    private utils: UtilsProvider,
    public storage: Storage) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CommercePage');
    this.getSecteur();
  }

  push(i){
    console.log(this.reponseData[i]);
    this.navCtrl.push('CommerceListPage',{secteur: this.reponseData[i]})
  }
  back(){
    this.navCtrl.pop();
  }

  getSecteur(){
    if(this.connectivityService.isOnline()){
      let loader = this.utils.makeLoader('loading...');
      loader.present().then(()=>{
        this.authService.getAllSecteur().then(response =>{
            loader.dismiss();
            this.reponseData = response.data;
            console.log(response);
            // this.storage.set('token', response.access_token);
            // this.storage.set('user', response.user);
            // this.storage.set('token_type', response.token_type);
            // this.navCtrl.setRoot(TabsPage,{});
            //  if(this.response.user.typeuser_id == '0'){
            //     this.navCtrl.setRoot('HomePage');
            //     this.storage.set('isLoggedIn', 'employeur');
            //   }
            //   else{
                // this.navCtrl.setRoot('HomeemployePage');
            //     this.storage.set('isLoggedIn', 'employe');
  
            //   }
        }).catch((error)=>{
          console.log(error);
  
          loader.dismiss();
        })
      })
    }
    else{
      this.utils.makeToast('Veuillez vous connecter à internet').present();
    }
  }
  
}
