import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CommercePage } from './commerce';

@NgModule({
  declarations: [
    CommercePage,
  ],
  imports: [
    IonicPageModule.forChild(CommercePage),
  ],
})
export class CommercePageModule {}
