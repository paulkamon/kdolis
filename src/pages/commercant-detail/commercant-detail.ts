import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the CommercantDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-commercant-detail',
  templateUrl: 'commercant-detail.html',
})
export class CommercantDetailPage {
  detail;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.detail=this.navParams.get('detail');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CommercantDetailPage');
  }

  back(){
    this.navCtrl.pop();
  }
}
