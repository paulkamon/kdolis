import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CommercantDetailPage } from './commercant-detail';

@NgModule({
  declarations: [
    CommercantDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(CommercantDetailPage),
  ],
})
export class CommercantDetailPageModule {}
