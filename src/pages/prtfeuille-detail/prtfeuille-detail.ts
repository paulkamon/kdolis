import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import JsBarcode from 'jsbarcode';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { ConnectivityProvider } from '../../providers/connectivity/connectivity';
import { UtilsProvider } from '../../providers/utils/utils';
import { Storage } from '@ionic/storage';


/**
 * Generated class for the PrtfeuilleDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-prtfeuille-detail',
  templateUrl: 'prtfeuille-detail.html',
})
export class PrtfeuilleDetailPage {

  public picture;
  @ViewChild('barcode') barcode: ElementRef;

card;

cardno;
solde;
forfait;
expiration;

scan;
data = {
  carteKdolisId: '',
  carteKdolisIsSecure: false,
  clientInfosId: ''
  };
  constructor(public navCtrl: NavController, public navParams: NavParams, public alertCtrl: AlertController, private connectivityService: ConnectivityProvider,
    private authService: AuthServiceProvider,
    private utils: UtilsProvider,public storage: Storage) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PrtfeuilleDetailPage');
    this.card = this.navParams.get('card');
    this.scan = this.navParams.get('scan');
    this.cardno = this.card.numCarte;
    this.solde = this.card.solde;
    this.forfait = this.card.forfait;

    var date1 =new Date(this.card.dateExpirated.replace(/-/g, '/'));
    var date2 = new Date();
    var Difference_In_Time = date1.getTime() - date2.getTime();
    this.expiration =Math.round(Difference_In_Time / (1000 * 3600 * 24));

    this.data.carteKdolisId = this.card.id
    JsBarcode(this.barcode.nativeElement, this.cardno, {
  height:40,
  displayValue: false
});
this.storage.get('userData').then((data)=>{
  this.data.clientInfosId = data.id;
});
  }

  back(){
    this.navCtrl.pop();
  }

  bindCard(){
    if(this.connectivityService.isOnline()){
      let loader = this.utils.makeLoader('');
      loader.present().then(()=>{
        this.authService.bindCard(this.data).then(response =>{
            loader.dismiss();
            console.log(response);
            if (response.code==1) {
              this.utils.makeToast(response.msg).present();
            }else{
              this.utils.makeToast('impossible '+response.msg).present();
            }
          
        }).catch((error)=>{
          console.log('erreur '+error);
  
          loader.dismiss();
          this.utils.makeToast('erreur!').present();
        })
      })
    }
    else{
      this.utils.makeToast('Veuillez vous connecter à internet').present();
    }
  }

  showConfirm() {
    console.log('showconfirm');
    const confirm = this.alertCtrl.create({
      // title: 'Deconnexion?',
      message: 'Ajouter la carte à votre portefeuille?',
      buttons: [
        {
          text: 'non',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'oui',
          handler: () => {
            console.log('Agree clicked');
            this.bindCard();
          }
        }
      ]
    });
    confirm.present();
  }
}
