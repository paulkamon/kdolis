import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PrtfeuilleDetailPage } from './prtfeuille-detail';

@NgModule({
  declarations: [
    PrtfeuilleDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(PrtfeuilleDetailPage),
  ],
})
export class PrtfeuilleDetailPageModule {}
