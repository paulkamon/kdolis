import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { ConnectivityProvider } from '../../providers/connectivity/connectivity';
import { UtilsProvider } from '../../providers/utils/utils';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the AchatPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-achat',
  templateUrl: 'achat.html',
})
export class AchatPage {
user;
reponseData:any[];

  constructor(public navCtrl: NavController, public navParams: NavParams, private connectivityService: ConnectivityProvider,
    private authService: AuthServiceProvider,
    private utils: UtilsProvider, public storage: Storage) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AchatPage');
    this.storage.get('userData').then((data)=>{
      this.user = data;
      this.getlistCard(this.user.id);
    });
  }

  public round(decimal){
    return Math.round(decimal);
  }

  back(){
    this.navCtrl.pop();
  }

  push(i){
    this.navCtrl.push('AchatDetailPage',{card: this.reponseData[i].card})
  }

  getlistCard(id){
    if(this.connectivityService.isOnline()){
      let loader = this.utils.makeLoader('loading...');
      loader.present().then(()=>{
        this.authService.getAchatCards(id).then(response =>{
            loader.dismiss();
            this.reponseData = response.data;
            console.log(response);
        }).catch((error)=>{
          console.log('error'+error);
  
          loader.dismiss();
        })
      })
    }
    else{
      this.utils.makeToast('Veuillez vous connecter à internet').present();
    }
  }
}
