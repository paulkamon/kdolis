import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AchatPage } from './achat';

@NgModule({
  declarations: [
    AchatPage,
  ],
  imports: [
    IonicPageModule.forChild(AchatPage),
  ],
})
export class AchatPageModule {}
