import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PrtfeuillePage } from './prtfeuille';

@NgModule({
  declarations: [
    PrtfeuillePage,
  ],
  imports: [
    IonicPageModule.forChild(PrtfeuillePage),
  ],
})
export class PrtfeuillePageModule {}
