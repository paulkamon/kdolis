import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { ConnectivityProvider } from '../../providers/connectivity/connectivity';
import { UtilsProvider } from '../../providers/utils/utils';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the PrtfeuillePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-prtfeuille',
  templateUrl: 'prtfeuille.html',
})
export class PrtfeuillePage {
user;
reponseData:any[];
data={
  buyerId: ''
};

  constructor(public navCtrl: NavController, public navParams: NavParams, private connectivityService: ConnectivityProvider,
    private authService: AuthServiceProvider,
    private utils: UtilsProvider, public storage: Storage) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PrtfeuillePage');
    this.storage.get('userData').then((data)=>{
      this.user = data;
      this.data.buyerId =data.id;
      this.getlistCard(this.data);
    });
  }

  back(){
    this.navCtrl.pop();
  }

  push(i){
    this.navCtrl.push('PrtfeuilleDetailPage',{card: this.reponseData[i].card})
  }

  getlistCard(data){
    if(this.connectivityService.isOnline()){
      let loader = this.utils.makeLoader('loading...');
      loader.present().then(()=>{
        this.authService.getCardsList(data).then(response =>{
            loader.dismiss();
            this.reponseData = response.data;
            console.log(response);
        }).catch((error)=>{
          console.log('error'+error);
  
          loader.dismiss();
        })
      })
    }
    else{
      this.utils.makeToast('Veuillez vous connecter à internet').present();
    }
  }
}
