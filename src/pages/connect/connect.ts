import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { ConnectivityProvider } from '../../providers/connectivity/connectivity';
import { UtilsProvider } from '../../providers/utils/utils';
import { Storage } from '@ionic/storage';
/**
 * Generated class for the ConnectPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-connect',
  templateUrl: 'connect.html',
})
export class ConnectPage {
  data = {
    login: '',
    password: ''
    };

  constructor(public navCtrl: NavController, public navParams: NavParams, private connectivityService: ConnectivityProvider,
    private authService: AuthServiceProvider,
    private utils: UtilsProvider,
    public storage: Storage,
    private alertCtrl: AlertController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ConnectPage');
  }
  register(){
    this.navCtrl.push("RegisterPage")
  }

  back(){
    this.navCtrl.pop();
  }

  // login(){
  //   this.navCtrl.push('DuserPage');
  // }

  
login() {
  if(this.connectivityService.isOnline()){
    let loader = this.utils.makeLoader('Connexion en cours...');
    loader.present().then(()=>{
      this.authService.login(this.data).then(response =>{
          loader.dismiss();
          // this.response = response;
          console.log(response);
          if (response.code==1) {
            if (response.data.realPassword != '1234') {
              this.storage.set('connexion', response.connexion).then(()=>{
                console.log('connexionstored');
            this.storage.set('userData', response.data.userInfos).then(()=>{
              console.log('userdatastored');
            this.storage.set('responseData', response.data).then(()=>{
                console.log('stored');
                  // this.navCtrl.setRoot(TabsPage,{});
                  if(response.data.usersRole_id == '1'){
                    this.navCtrl.setRoot('UuserPage');
                  }
                  if(response.data.usersRole_id == '2'){
                    this.navCtrl.setRoot('DuserPage');
                  }
                  if(response.data.usersRole_id == '3'){
                    this.navCtrl.setRoot('CuserPage');
                  }
  
              });
            });
          });
            } else {
              if (response.data.usersRole_id == '1') {
                this.storage.set('connexion', response.connexion).then(()=>{
                  console.log('connexionstored');
              this.storage.set('userData', response.data.userInfos).then(()=>{
                console.log('userdatastored');
              this.storage.set('responseData', response.data).then(()=>{
                  console.log('stored');
                      this.navCtrl.setRoot('UuserPage');
                });
              });
            });              }
              this.navCtrl.push('UpdatdePasswordPage',{response: response})
            }
            
      }else{
            this.utils.makeToast(response.msg).present();
          }
        
      }).catch((error)=>{
        console.log(error);

        loader.dismiss();
        this.utils.makeToast('Votre login ou votre mot passe est incorrecte!').present();
      })
    })
  }
  else{
    this.utils.makeToast('Veuillez vous connecter à internet').present();
  }
}
}
