import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CommerceListPage } from './commerce-list';

@NgModule({
  declarations: [
    CommerceListPage,
  ],
  imports: [
    IonicPageModule.forChild(CommerceListPage),
  ],
})
export class CommerceListPageModule {}
