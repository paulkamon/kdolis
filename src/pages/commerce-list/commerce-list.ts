import { Component, NgZone } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { ConnectivityProvider } from '../../providers/connectivity/connectivity';
import { UtilsProvider } from '../../providers/utils/utils';
/**
 * Generated class for the CommerceListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-commerce-list',
  templateUrl: 'commerce-list.html',
})
export class CommerceListPage {
title;
secteur;
id;
reponseData:any[];
filterData:any[];
searchTerm: string = '';

public isSearchbarOpened=false;
public showIcon = false;

  constructor(public navCtrl: NavController, public navParams: NavParams, private connectivityService: ConnectivityProvider,
    private authService: AuthServiceProvider,
    private utils: UtilsProvider, public zone: NgZone) {

    this.secteur = this.navParams.get('secteur');
    this.title = this.secteur.label;
    this.id = this.secteur.id;
    if (this.id) {
      this.getlistShop();
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CommerceListPage');
  }

  getlistShop(){
    if(this.connectivityService.isOnline()){
      let loader = this.utils.makeLoader('loading...');
      loader.present().then(()=>{
        this.authService.getAllSeller(this.id).then(response =>{
            loader.dismiss();
            this.reponseData = response.data;
            this.filterData = this.reponseData;
            console.log(response);
        }).catch((error)=>{
          console.log(error);
  
          loader.dismiss();
        })
      })
    }
    else{
      this.utils.makeToast('Veuillez vous connecter à internet').present();
    }
  }
  push(i){
    this.navCtrl.push('CommercantDetailPage',{detail: this.filterData[i]});
  }
  back(){
    this.navCtrl.pop();
  }
   
  onSearch(event){
    console.log(event.target.value);
  }

  getItems(ev: any) {
    console.log('ev '+ev.target.value);

    // set val to the value of the searchbar
    const val = ev.target.value;

    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      this.filterData = this.reponseData.filter((item) => {
        return (item.nomBoutique.toLowerCase().indexOf(this.searchTerm.toLowerCase()) > -1) || (item.adresseBoutique.toLowerCase().indexOf(this.searchTerm.toLowerCase()) > -1) || (item.telephoneBoutique.toLowerCase().indexOf(this.searchTerm.toLowerCase()) > -1);
      })
    }
  }

  onCancel(event){
    console.log('oncancel '+event.target.value);
    this.isSearchbarOpened=false;
    this.filterData = this.reponseData;
  }

  scrollHandler(event){
    console.log(`ScrollEvent: ${event}`)
    console.log('evScrollEvent ');
    this.showIcon=true;
    this.zone.run(()=>{
      // since scrollAmount is data-binded,
      // the update needs to happen in zone
      this.showIcon=true;
    })
  }

}
